<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userdata= DB::select('SELECT * FROM users WHERE id='.Auth::user()->id.';');
        // var_dump($userdata[0]->name);die;
        session()->put('name', $userdata[0]->name);
        session()->put('email', $userdata[0]->name);
        $data['title']="Dashboard";
        return view('home')->with($data);
    }
}
