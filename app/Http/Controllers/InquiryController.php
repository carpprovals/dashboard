<?php

namespace App\Http\Controllers;

use App\Inquiry;
use DB;
use Illuminate\Http\Request;

class InquiryController extends Controller
{
    public function __construct()
    {
        
          $this->middleware('auth')->only(['statusofdata']);
          
		  $this->middleware('auth')->only(['updatestatus']);
		  $this->middleware('auth')->only(['index']);
    }
    public function index()
    {

        $inquiry = Inquiry::all();
        $title="Inquiry";
        return view('inquiry.index')->with('inquiry', $inquiry)->with('title', $title);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inquiry = new Inquiry();
        $inquiry->message = $request->comment;
        $inquiry->phoneno = $request->mobileno;
        $inquiry->name = $request->name;
        $inquiry->email = $request->email;
        $inquiry->lead_status = 'Pending';
        $inquiry->save();
        return redirect('/')->with('status', 'Inquiry Created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inquiry  $inquiry
     * @return \Illuminate\Http\Response
     */
    public function show(Inquiry $inquiry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inquiry  $inquiry
     * @return \Illuminate\Http\Response
     */
    public function edit(Inquiry $inquiry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inquiry  $inquiry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inquiry $inquiry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inquiry  $inquiry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inquiry $inquiry)
    {
        //
    }


    public function statusofdata(Request $request)
    {
        if($request->str==''){
            $inquiry = DB::table('inquiry')
           ->get();
        }else{
            $inquiry = DB::table('inquiry')
            ->where('lead_status', '=', $request->str)
           ->get();
        }
       

        return view('inquiry.show')->with('inquiry', $inquiry);
          
    }

    public function updatestatus(Request $request)
    {
        $inquiry = Inquiry::find($request->id);
   
        $inquiry->lead_status =$request->value;
    $inquiry->save();

        return 1;
          
    }
}
