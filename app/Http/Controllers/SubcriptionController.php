<?php

namespace App\Http\Controllers;

use App\Subcription;
use Illuminate\Http\Request;

class SubcriptionController extends Controller
{
    public function __construct()
    {
        
		  $this->middleware('auth')->only(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
        $Subcription = Subcription::all();
        $title="Subcription";
        return view('subcription.index')->with('Subcription', $Subcription)->with('title', $title);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subcription = new Subcription();
        $subcription->email = $request->subcripemail;
       
        $subcription->save();
        return redirect('/')->with('status', 'Subcription Created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subcription  $subcription
     * @return \Illuminate\Http\Response
     */
    public function show(Subcription $subcription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subcription  $subcription
     * @return \Illuminate\Http\Response
     */
    public function edit(Subcription $subcription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subcription  $subcription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subcription $subcription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subcription  $subcription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subcription $subcription)
    {
        //
    }
}
