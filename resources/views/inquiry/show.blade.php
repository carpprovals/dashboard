<div class="card-body">
										<!--begin: Datatable-->
										<table class="table table-bordered table-hover table-checkable" id="example" style="margin-top: 13px !important">
											<thead>
												<tr>
                        <th scope="col">Name</th>
    <th scope="col">Phone No</th>
    <th scope="col">email</th>
    <th scope="col">message</th>
	<th scope="col">Status</th>
    <th scope="col">Action</th>
												</tr>
                                            </thead>
                                            <tbody>
 
 @foreach($inquiry as $inquiry_rec)
 
   <tr>
     <td> {{$inquiry_rec->name}} </td>
     <td>{{$inquiry_rec->phoneno}}</td>
     <td>{{$inquiry_rec->email}}</td>
     <td>{{$inquiry_rec->message}}</td>
	 <td>{{$inquiry_rec->lead_status}}</td>
     <td>
	 <select class="form-control" id="type" name="quantity" onchange="updatestatus(this.value,<?php echo $inquiry_rec->id?>);">
    <option disabled value="">Select Status</option>
    <option value="Pending" {{ $inquiry_rec->lead_status == 'Pending' ? 'selected' : '' }}>Pending</option>
    <option value="Cold" {{ $inquiry_rec->lead_status == 'Cold' ? 'selected' : '' }}>Cold</option>
    <option value="Parking" {{ $inquiry_rec->lead_status == 'Parking' ? 'selected' : '' }}>Parking</option>
    <option value="Warm" {{ $inquiry_rec->lead_status == 'Warm' ? 'selected' : '' }}>Warm</option>
    <option value="Hot" {{ $inquiry_rec->lead_status == 'Hot' ? 'selected' : '' }}>Hot</option>
</select>
    </td>
   </tr>
   @endforeach
 </tbody>
										</table>
										<!--end: Datatable-->
									</div>