@extends('layouts.admin')

@section('content')

<div class="content-header row">
        </div>
        <div class="content-body"><!-- Dashboard Analytics Start -->
<section id="dashboard-analytics">
<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<!--begin::Notice-->
								
								<!--end::Notice-->
								<!--begin::Card-->
								<div class="card card-custom">
									<div class="card-header">
									
										<div class="card-toolbar">
                    <div class="row">
											<!--begin::Dropdown-->
											<div class="col-md--2">
											<a href="{{ url('inquiry') }}"><button type="button" class="btn btn-light-primary font-weight-bolder"  aria-haspopup="true" aria-expanded="false">
												All</button></a>
											</div>
											<div class="col-md--2">
												<button type="button" class="btn btn-light-primary font-weight-bolder" aria-haspopup="true" id="Cold" aria-expanded="false" onclick="statusdatatable('Cold');">
												Cold</button>
											</div>
                      <div class="col-md--2">
												<button type="button" class="btn btn-light-primary font-weight-bolder" aria-haspopup="true" aria-expanded="false"  id="Parking"  onclick="statusdatatable('Parking');">
												Parking</button>
											</div>
                      <div class="col-md--2">
												<button type="button" class="btn btn-light-primary font-weight-bolder" aria-haspopup="true" aria-expanded="false"  id="Hot"  onclick="statusdatatable('Hot');">
												Hot</button>
											</div>
                      <div class="col-md--2">
												<button type="button" class="btn btn-light-primary font-weight-bolder" aria-haspopup="true" aria-expanded="false" id="Pending"  onclick="statusdatatable('Pending');">
												Pending</button>
											</div>
										  <div class="col-md--2">
												<button type="button" class="btn btn-light-primary font-weight-bolder" aria-haspopup="true" aria-expanded="false" id="Warm"  onclick="statusdatatable('Warm');">
												Warm</button>
											</div>
										</div>
                    </div>
									</div>
                  <input type="hidden" id="status">
									<div class="card-body" id="statusdatatablelist">
										<!--begin: Datatable-->
										<table class="table table-bordered table-hover table-checkable" id="example" style="margin-top: 13px !important">
											<thead>
												<tr>
                        <th scope="col">Name</th>
    <th scope="col">Phone No</th>
    <th scope="col">email</th>
    <th scope="col">message</th>
    <th scope="col">Status</th>
    <th scope="col">Action</th>
												</tr>
                                            </thead>
                                            <tbody>
 
 @foreach($inquiry as $inquiry_rec)
 
   <tr>
     <td> {{$inquiry_rec->name}} </td>
     <td>{{$inquiry_rec->phoneno}}</td>
     <td>{{$inquiry_rec->email}}</td>
     <td>{{$inquiry_rec->message}}</td>
     <td>{{$inquiry_rec->lead_status}}</td>
     <td>
     <select class="form-control" id="type" name="quantity" onchange="updatestatus(this.value,<?php echo $inquiry_rec->id?>);">
    <option disabled value="">Select Status</option>
    <option value="Pending" {{ $inquiry_rec->lead_status == 'Pending' ? 'selected' : '' }}>Pending</option>
    <option value="Cold" {{ $inquiry_rec->lead_status == 'Cold' ? 'selected' : '' }}>Cold</option>
    <option value="Parking" {{ $inquiry_rec->lead_status == 'Parking' ? 'selected' : '' }}>Parking</option>
    <option value="Warm" {{ $inquiry_rec->lead_status == 'Warm' ? 'selected' : '' }}>Warm</option>
    <option value="Hot" {{ $inquiry_rec->lead_status == 'Hot' ? 'selected' : '' }}>Hot</option>
</select>
    </td>
   </tr>
   @endforeach
 </tbody>
										</table>
										<!--end: Datatable-->
									</div>
								</div>
								<!--end::Card-->
							</div>
							<!--end::Container-->
						</div>
  <!--/ List DataTable -->
</section>
<!-- Dashboard Analytics end -->

        </div>
@endsection
