@extends('layouts.admin')

@section('content')

<div class="content-header row">
        </div>
        <div class="content-body"><!-- Dashboard Analytics Start -->
<section id="dashboard-analytics">
<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<!--begin::Notice-->
								
								<!--end::Notice-->
								<!--begin::Card-->
								<div class="card card-custom">
								
									<div class="card-body" id="statusdatatablelist">
										<!--begin: Datatable-->
										<table class="table table-bordered table-hover table-checkable" id="example" style="margin-top: 13px !important">
											<thead>
												<tr>
                        <th scope="col">Email</th>
												</tr>
                                            </thead>
                                            <tbody>
 
 @foreach($Subcription as $Subcription_rec)
 
   <tr>
     <td> {{$Subcription_rec->email}} </td>
   </tr>
   @endforeach
 </tbody>
										</table>
										<!--end: Datatable-->
									</div>
								</div>
								<!--end::Card-->
							</div>
							<!--end::Container-->
						</div>
  <!--/ List DataTable -->
</section>
<!-- Dashboard Analytics end -->

        </div>
@endsection
