<!doctype html>
<html class="no-js" lang="zxx">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Carpproval</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="manifest" href="site.html">
		<!-- <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico"> -->
		<link rel="stylesheet"  href="{{ asset('public/website-asset/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('public/website-asset/css/bootstrap.min.css') }}" href="assets/css/animate.min.css">
		<!-- <link rel="stylesheet" href="assets/css/style.css"> -->
		<link rel="stylesheet" href="{{ asset('public/website-asset/css/index.css') }}"href="assets/">
		<link rel="stylesheet" href="{{ asset('public/website-asset/css/common.css') }}"href="assets/">
		<link rel="stylesheet" href="{{ asset('public/website-asset/css/responsive.css') }}"href="assets">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
	</head>

	<body class="container-xl">

		<!-- <div id="preloader-active">
			<div class="preloader d-flex align-items-center justify-content-center">
				<div class="preloader-inner position-relative">
					<div class="preloader-circle"></div>
					<div class="preloader-img pere-text">
						<!-- <img src="assets/img/logo/logo.png" alt=""> --
					</div>
				</div>
			</div>
		</div> -->

		<header class=" fixed-top">
		
			<nav class="navbar navbar-expand d-flex" >
				<a class="navbar-brand pb-0 pt-0"  href="{{ URL::to('/')}}"><img class="logo" src="{{ asset('public/website-asset/img/logo.png') }}" alt=""></a>

				<div class=" location-div d-flex justify-content-end" >
					<div class="d-flex mr-5" >
						<img class="header-mail-icon" src="{{ asset('public/website-asset/img/mail_white.png') }}">
						<p class="header-website m-0">info@carapprovals.com</p>
					</div>
					<div class="d-flex mr-5" >
						<img class="header-location-icon" src="{{ asset('public/website-asset/img/location_white.png') }}">
						<p class="header-contact m-0">1-5250 Satellite Dr Mississauga, On L4W 5G5</p>
					</div>
				</div>
			</nav>
      @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
			<div id="enquiryDiv" data-toggle="modal" data-target="#enquiryModal">
				<p>E</p>
				<p>N</p>
				<p>Q</p>
				<p>U</p>
				<p>I</p>
				<p>R</p>
				<p>y</p>
			</div>


		</header>

		<main>

			<br><br><br>

			<section id="section1">
				<div class="row">
					<div class="col-6  col-sm-7 col-lg-7 pr-0"></div>
					<div class="col-6 col-sm-5 col-lg-5 p-0">
						<div id="bookYourCarDiv">
							<p id="bannerTitle" >Find Your Perfect Car</p>
							<p id="bannerContent">Lorem ipsum dolor sit amet, consetetur sadipscing elitr,<br> sed diam nonumy eirmod tempor</p>

							<button type="button" class="btn book-car-btn">BOOK YOUR CAR</button>
						</div>
				
					</div>
				</div>
			</section>

			<!-- our best offer -->
			<section id="section2">
				<div class="row justify-content-center">
					<div class="col-12 col-md-6 section-col-1">
						<div>
							<img class="section-2-img" src="{{ asset('public/website-asset/img/img1.png') }}">

							<p class="section-2-title">THE NEW WAY TO BUY A CAR</p>
							<p class="section-2-content" >Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor</p>
						</div>
					</div>

					<div class="col-12 col-md-6 section-col-2">
						<div>
							<img class="section-2-img" src="{{ asset('public/website-asset/img/img2.png') }}">

							<p class="section-2-title">SELL OR TRADE YOUR CAR</p>
							<p class="section-2-content" >Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor</p>
						</div>
					</div>
				</div>

				<div class="row mb-5">
					<div class="col-12">
						<p id="ourBestOfferTitle">OUR BEST <span>OFFERS</span> AND <span>QUALITY</span></p>
						<p id="ourBestOfferContent">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
					</div>
				</div>

				<div class="row mt-5  justify-content-center">
					<div class="col-12 order-2 order-md-1 col-md-4">

						<div class="row">
							<div class="col-12 d-flex offer-col">
								<img class="offer-icon" src="{{ asset('public/website-asset/img/features1.png') }}">
								<div class="pl-3">
									<p class="offer-title">Lorem ipsum</p>

									<p class="offer-content">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
								</div>
							</div>

							<div class="col-12 d-flex offer-col">
								<img class="offer-icon" src="{{ asset('public/website-asset/img/features1.png') }}">
								<div class="pl-3">
									<p class="offer-title">Lorem ipsum</p>

									<p class="offer-content">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
								</div>
							</div>

							<div class="col-12 d-flex offer-col">
								<img class="offer-icon" src="{{ asset('public/website-asset/img/features1.png') }}">
								<div class="pl-3">
									<p class="offer-title">Lorem ipsum</p>

									<p class="offer-content">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
								</div>
							</div>
						</div>
						
					</div>

					<div class="col-12 order-1 order-md-2 col-md-3">
						<img class="our-offer-car" src="{{ asset('public/website-asset/img/features.png') }}" width="100%;" >
					</div>

					<div class="col-12 order-3 order-md-3 col-md-4">

						<div class="row">
							<div class="col-12 d-flex offer-col">
								<img class="offer-icon" src="{{ asset('public/website-asset/img/features1.png') }}">
								<div class="pl-3">
									<p class="offer-title">Lorem ipsum</p>

									<p class="offer-content">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
								</div>
							</div>

							<div class="col-12 d-flex offer-col">
								<img class="offer-icon" src="{{ asset('public/website-asset/img/features1.png') }}">
								<div class="pl-3">
									<p class="offer-title">Lorem ipsum</p>

									<p class="offer-content">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
								</div>
							</div>

							<div class="col-12 d-flex offer-col">
								<img class="offer-icon" src="{{ asset('public/website-asset/img/features1.png') }}">
								<div class="pl-3">
									<p class="offer-title">Lorem ipsum</p>

									<p class="offer-content">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- popular cars -->
			<section id="section3">
				<p id="popularCarTitle">Popular <span>Cars</span></p>

				<div class="row justify-content-center">
					<div class="col-11">

						<p id="popularCarContentTitle">Lorem ipsum dolor sit amconsetetur sadipscing el<br> consetetur sadipscing elitr, sed duo dolores et ea</p>
						<div class="row">
							

							<div class="col-12 col-lg-6 order-2 order-md-1">
								<p class="popular-car-content">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat</p>
							</div>

							<div class="col-12 col-lg-6 order-1 order-md-2">
								<img class="popular-car-img" src="{{ asset('public/website-asset/img/mobile.png') }}" width="70%" >
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- help center  -->
			<section id="section4">
				<div class="row justify-content-center">
					<div class="col-12 col-md-5 col-lg-4 help-center-col pl-4">
						<div class="d-flex">
							<img class="help-center-icon" src="{{ asset('public/website-asset/img/24-7.png') }}">
							<p class="help-center-title pl-3">Help <br>Center</p>
						</div>

						<p class="help-center-content" >Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum</p>
					</div>
					<div class="col-12 col-md-2 ">
						<div class="border-div"></div>
					</div>
					<div class="col-12 col-md-5 col-lg-4 help-center-col">
						<div class="d-flex">
							<img class="help-center-icon" src="{{ asset('public/website-asset/img/best service.png') }}">
							<p class="best-service-title pl-3">Best <br> Managed <br> Companies</p>
						</div>

						<p class="help-center-content" >Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum</p>
					</div>
				</div>
			</section>

			<!-- customer reviews -->
			<section id="section5">
				<p id="customeReviewTitle">Customer <span>Reviews</span></p>

				<div class="row justify-content-center">
					<div class="col-11">
						<div class="row justify-content-center">
							<div class="col-12 col-md-6 col-lg-3">
								<div class="card customer-review-card">
									<div class="card-body pb-0">
										<div class="row justify-content-center m-0">
											<div class="col-lg-11 d-flex star-img-div">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
											</div>

											<div class="col-12">
												<p class="customer-reviews-content"><span>Lorem ipsum dolor sit amet,</span> consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-12 col-md-6 col-lg-3">
								<div class="card customer-review-card">
									<div class="card-body pb-0">
										<div class="row justify-content-center m-0">
											<div class="col-lg-11 d-flex star-img-div">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
											</div>

											<div class="col-12">
												<p class="customer-reviews-content"><span>Lorem ipsum dolor sit amet,</span> consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-12 col-md-6 col-lg-3">
								<div class="card customer-review-card">
									<div class="card-body pb-0">
										<div class="row justify-content-center m-0">
											<div class="col-lg-11 d-flex star-img-div">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
											</div>

											<div class="col-12">
												<p class="customer-reviews-content"><span>Lorem ipsum dolor sit amet,</span> consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-12 col-md-6 col-lg-3">
								<div class="card customer-review-card">
									<div class="card-body pb-0">
										<div class="row justify-content-center m-0">
											<div class="col-lg-11 d-flex star-img-div">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
												<img class="star-img" src="{{ asset('public/website-asset/img/star.png') }}">
											</div>

											<div class="col-12">
												<p class="customer-reviews-content"><span>Lorem ipsum dolor sit amet,</span> consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- <div id="chatbot">
				<img src="assets/img/chatbot.png" width="100%">
			</div> -->

		</main>

		<footer>
		<div class="row justify-content-center" id="moreInfoDiv">
				<div class="col-10" id="moreInfoCol" >
					<img src="{{ asset('public/website-asset/img/moreInfo.png') }}">
					<p id="moreInfoTitle" >FOR MORE INFORMATION</p>
          <form method="POST" action="{{ URL::to('/subcription')}}"  >
                        @csrf
						<div class="row justify-content-center">
							<!-- <div class="col-1"></div> -->
							<div class="col-7">
								<input type="email" required class="form-control moreInfoInput" placeholder="Enter your Email ID" name="subcripemail">
							</div>
							<div class="col-3">
								<button type="submit" class="btn moreInfoButton" >SUBMIT</button>
							</div>
							<!-- <div class="col-1"></div> -->
						</div>
					</form>

					
				</div>
			</div>
			<div class="row justify-content-center m-0">
				<div class="col-12 col-lg-10">
					<div class="row footer-content justify-content-around">
						<div class="col-12 col-sm-4">
							<p class="footer-title">About Carapproval</p>
							<p class="about-carapproval"><span>Lorem ipsum dolor sit amet,</span> consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
			
						</div>
						<div class="col-12 col-sm-4 pl-5">
							<p class="footer-title">Quick links</p>
							<p class="footer-link">About Us</p>
							<p class="footer-link">Contact Us</p>
							<p class="footer-link">Terms & Condition</p>
						</div>
						<div class="col-12 col-sm-4 col-md-3">
							<p class="footer-title">Follow Us</p>
							<div class="d-flex justify-content-end p-0">
								<img class="social-media-icon" src="{{ asset('public/website-asset/img/facebook.png') }}">
								<img class="social-media-icon" src="{{ asset('public/website-asset/img/youtube.png') }}">
								<img class="social-media-icon" src="{{ asset('public/website-asset/img/linkedin.png') }}">
								<img class="social-media-icon" src="{{ asset('public/website-asset/img/Instagram.png') }}">
							</div>
						</div>
					</div>
				</div>

				<div class="col-12 d-flex footer-black justify-content-between">
					<p class="black-footer-contact">&copy; All Right Reserve CARAPPROVALS</p>
					<p class="black-footer-contact"> <a href="privacy_policy.html" class="privacy-policy-link">Privacy Policy</a></p>
				</div>
			</div>
		</footer>

		<!-- enquiry Modal -->

		<div class="modal" id="enquiryModal">
			<div class="modal-dialog">
				<div class="modal-content">

					<!-- Modal body -->
					<div class="modal-body">
          <form method="POST" action="{{ URL::to('/inquiry')}}" onsubmit="return validatefunction();" >
                        @csrf
							<div class="row justify-content-center">
								<div class="col-11 modal-border-bottom">
									<p class="modal-title">Enquiry</p>
								</div>

								<div class="col-11 mb-3">
									<input type="text" required class="form-control" placeholder="Enter your name" name="name" id="name">
								</div>

								<div class="col-11 mb-3">
									<input type="email" required class="form-control" placeholder="Enter your Email ID" name="email" id="email">
								</div>

								<div class="col-11 mb-3">
									<input type="text" required class="form-control" placeholder="Enter your Mobile no." name="mobileno" id="mobileno">
								</div>

								<div class="col-11">
									<textarea class="form-control" required rows="7" placeholder="Write message here ..." name="comment" id="comment"></textarea>
								</div>

								<div class="col-11">
									<button type="submit" class="btn modal-btn btn-block" >Send</button>
								</div>
							</div>
						</form>
					</div>

				</div>
			</div>
		</div>
		<!--end enquiry Modal -->



		<script src="{{ asset('public/website-asset/js/vendor/jquery-1.12.4.min.js') }}"></script>
		<script src="{{ asset('public/website-asset/js/popper.min.js') }}"></script>
		<script src="{{ asset('public/website-asset/js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('public/website-asset/js/animated.headline.js') }}"></script>
		<script src="{{ asset('public/website-asset/js/index.js') }}"></script>

		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
		<script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: 'd02c12f2-e78f-4cc4-b3a5-b0be461d0e8e', f: true }); done = true; } }; })();</script>
	<script>
function validatefunction(){
  var value=document.getElementById("mobileno").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  return true;
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("mobileno").value='';
	   return false;
     }

}

  </script>
  
  </body>

</html>