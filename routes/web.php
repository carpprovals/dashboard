<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', function () {
    return view('auth.login');
});

Auth::routes();
Route::resource('inquiry', 'InquiryController');
Route::get('/home', 'HomeController@index')->name('home')->middleware(['auth']);
Route::get('/validatenum/statusofdata',"InquiryController@statusofdata")->name('validatenum.statusofdata');
Route::resource('subcription', 'SubcriptionController');
Route::get('/validate/updatestatus',"InquiryController@updatestatus")->name('validate.updatestatus');
Route::get('/', function () {
    return view('website');
});